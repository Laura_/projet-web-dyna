<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function index()
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
    /**
     * @Route("/science", name="science")
     */
    public function science()
    {
        return $this->render('home/science.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
    /**
     * @Route("/guerre", name="guerre")
     */
    public function guerre()
    {
        return $this->render('home/guerre.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
    /**
     * @Route("/romance", name="romance")
     */
    public function romance()
    {
        return $this->render('home/romance.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
    /**
     * @Route("/classique", name="classique")
     */
    public function classique()
    {
        return $this->render('home/classique.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
    /**
     * @Route("/addcom", name="addcom")
     */
    public function addcom()
    {
        return $this->render('home/addcom.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
    /**
     * @Route("/connect", name="connect")
     */
    public function connect()
    {
        return $this->render('home/connect.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
    /**
     * @Route("pagelivre", name="pagelivre")
     */
    public function pagelivre()
    {
        return $this->render('home/pagelivre.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
    /**
     * @Route("/password", name="password")
     */
    public function password()
    {
        return $this->render('home/password.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
    /**
     * @Route("/posts", name="posts")
     */
    public function posts()
    {
        return $this->render('home/posts.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
    /**
     * @Route("/search", name="search")
     */
    public function search()
    {
        return $this->render('home/search.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
    /**
     * @Route("/signin", name="signin")
     */
    public function signin()
    {
        return $this->render('home/signin.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
    /**
     * @Route("/user", name="user")
     */
    public function user()
    {
        return $this->render('home/user.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
}
